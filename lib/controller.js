const logger = require('./logger');

const ALLOWED_METHODS = ['get', 'post', 'put', 'delete', 'patch', 'options'];

module.exports = (name, domain) => {
  const acceptableMethods = Object.keys(domain).reduce((acc, key) => {
    if (
      ALLOWED_METHODS.indexOf(key) > -1 &&
      typeof domain[key] === 'function'
    ) {
      logger.info('  > %s %s', key.toUpperCase(), name);
      acc.push(key);
    } else {
      logger.warning('  * %s %s', key.toUpperCase(), name);
    }
    return acc;
  }, []);

  const controller = {};

  controller[name.charAt(0) === '/' ? name.substring(1) : name] = (
    data,
    callback,
  ) => {
    if (acceptableMethods.indexOf(data.method) > -1) {
      domain[data.method](data, callback);
    } else {
      callback(405);
    }
  };
  return controller;
};
