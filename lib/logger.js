const util = require('util');

const RED = '\x1b[31m';
const GREEN = '\x1b[32m';
const YELLOW = '\x1b[33m';
const BLUE = '\x1b[34m';
const WHITE = '\x1b[37m';
const RESET = '\x1b[0m';

function _log(color, format, ...args) {
  console.log(
    '[%s] - %s%s%s',
    new Date().toISOString(),
    color,
    util.format(format, ...args),
    RESET,
  );
}

module.exports = {
  info(format, ...args) {
    _log(WHITE, format, ...args);
  },

  debug(format, ...args) {
    _log(GREEN, format, ...args);
  },

  warning(format, ...args) {
    _log(YELLOW, format, ...args);
  },

  error(format, ...args) {
    _log(RED, format, ...args);
  },
};
