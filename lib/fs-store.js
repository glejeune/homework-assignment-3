const fs = require('fs');
const path = require('path');
const helpers = require('./helpers');

const BASE_DIR = path.join(__dirname, '..', '.data');

module.exports = {
  // Create a file
  create(dir, file, data, callback) {
    const dataPath = path.join(BASE_DIR, dir);
    helpers.createIfNotExists(dataPath, err => {
      if (!err) {
        fs.open(
          path.join(dataPath, `${file}.json`),
          'wx',
          (err, fileDescriptor) => {
            if (!err && fileDescriptor) {
              const stringData = JSON.stringify(data);
              fs.writeFile(fileDescriptor, stringData, err => {
                if (!err) {
                  fs.close(fileDescriptor, callback);
                } else {
                  helpers.syncCloseNoError(fileDescriptor);
                  callback(err);
                }
              });
            } else {
              callback(err);
            }
          },
        );
      } else {
        callback(err);
      }
    });
  },

  // Read data from a file
  read(dir, file, callback) {
    fs.readFile(
      path.join(BASE_DIR, dir, `${file}.json`),
      'utf8',
      (err, data) => {
        if (!err && data) {
          const parsedData = helpers.jsonToObject(data);
          callback(false, parsedData);
        } else {
          callback(err, data);
        }
      },
    );
  },

  // Update data in a file
  update(dir, file, data, callback) {
    fs.open(
      path.join(BASE_DIR, dir, `${file}.json`),
      'r+',
      (err, fileDescriptor) => {
        if (!err && fileDescriptor) {
          const stringData = JSON.stringify(data);
          fs.ftruncate(fileDescriptor, err => {
            if (!err) {
              fs.writeFile(fileDescriptor, stringData, err => {
                if (!err) {
                  fs.close(fileDescriptor, callback);
                } else {
                  helpers.syncCloseNoError(fileDescriptor);
                  callback(err);
                }
              });
            } else {
              helpers.syncCloseNoError(fileDescriptor);
              callback(err);
            }
          });
        } else {
          callback(err);
        }
      },
    );
  },

  // Delete a file
  delete(dir, file, callback) {
    fs.unlink(path.join(BASE_DIR, dir, `${file}.json`), err => {
      if (err && err.code == 'ENOENT') {
        callback(false);
      } else {
        callback(err);
      }
    });
  },
};
