const fs = require('fs');
const path = require('path');

const MIMETYPES = {
  '.aac': 'audio/aac',
  '.avi': 'video/x-msvideo',
  '.bz': 'application/x-bzip',
  '.bz2': 'application/x-bzip2',
  '.css': 'text/css',
  '.eot': 'application/vnd.ms-fontobject',
  '.gif': 'image/gif',
  '.htm': 'text/html',
  '.html': 'text/html',
  '.ico': 'image/x-icon',
  '.jpeg': 'image/jpeg',
  '.jpg': 'image/jpeg',
  '.js': 'application/javascript',
  '.json': 'application/json',
  '.mpeg': 'video/mpeg',
  '.oga': 'audio/ogg',
  '.ogv': 'video/ogg',
  '.ogx': 'application/ogg',
  '.otf': 'font/otf',
  '.png': 'image/png',
  '.pdf': 'application/pdf',
  '.svg': 'image/svg+xml',
  '.swf': 'application/x-shockwave-flash',
  '.ttf': 'font/ttf',
  '.wav': 'audio/x-wav',
  '.weba': 'audio/webm',
  '.webm': 'video/webm',
  '.webp': 'image/webp',
  '.woff': 'font/woff',
  '.woff2': 'font/woff2',
  '.xhtml': 'application/xhtml+xml',
  '.xml': 'application/xml',
  '.3gp': 'video/3gpp',
};

module.exports = {
  createIfNotExists(path, callback) {
    if (fs.existsSync(path)) {
      callback(false);
    } else {
      fs.mkdir(path, {recursive: true}, callback);
    }
  },

  syncCloseNoError(fd) {
    try {
      fs.closeSync(fd);
    } catch (_) {}
  },

  jsonToObject(string) {
    try {
      return JSON.parse(string);
    } catch (e) {
      return {};
    }
  },

  contentType(filename) {
    return MIMETYPES[path.extname(filename)] || 'application/json';
  },
};
