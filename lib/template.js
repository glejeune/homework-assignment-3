const fs = require('fs');
const path = require('path');
const helpers = require('./helpers');
const config = require('../app/config');

const PARTIAL_REGEX = /{:\s*([^\s}]*)\s*}/g;
const VALUE_REGEX = /{[^:]\s*([^\s}]*)\s*}/g;

function interpolateValues(data, params) {
  let value;
  while ((value = VALUE_REGEX.exec(data)) !== null) {
    const valueReplace = value[0];
    const valueKey = value[1].split('.');
    const finalValue = valueKey.reduce((acc, key) => {
      if (typeof acc === 'object' && acc !== null && acc[key]) {
        return acc[key];
      }
      return valueReplace;
    }, params);
    data = data.replace(valueReplace, finalValue);
  }
  return data;
}

function interpolate(from, file, params, headers, callback, options) {
  const fileName = path.join(from, file);
  fs.readFile(fileName, {flag: 'r'}, (err, data) => {
    if (err) {
      if (options.httpREsponse) {
        callback(404, undefined, headers);
      } else {
        callback(err, undefined, headers);
      }
      return;
    }

    if (options.interpolate) {
      let partial;
      const renderers = [];
      while ((partial = PARTIAL_REGEX.exec(data)) !== null) {
        const partialFrom = path.dirname(fileName);
        const partialReplace = partial[0];
        const partialFile = partial[1];
        renderers.push(
          new Promise((resolve, reject) => {
            interpolate(
              partialFrom,
              partialFile,
              params,
              headers,
              (err, partialData, headers) => {
                if (err) {
                  const partialTemplate = path.join(partialFrom, partialFile);
                  reject(new Error(partialTemplate));
                } else {
                  resolve([partialReplace, partialData]);
                }
              },
              {...options, httpREsponse: false},
            );
          }),
        );
      }
      Promise.all(renderers)
        .then(partials => {
          let dataStr = data.toString();
          partials.forEach(([partialReplace, partialData]) => {
            dataStr = dataStr.replace(partialReplace, partialData.toString());
          });
          dataStr = interpolateValues(dataStr, params);
          if (options.httpREsponse) {
            callback(200, dataStr, headers);
          } else {
            callback(false, dataStr, headers);
          }
        })
        .catch(errors => {
          if (options.httpREsponse) {
            callback(500, undefined, headers);
          } else {
            callback(new Error(fileName), undefined, headers);
          }
        });
    } else {
      if (options.httpREsponse) {
        callback(200, data, headers);
      } else {
        callback(false, data, headers);
      }
    }
  });
}

module.exports = {
  genericRender(
    file,
    params,
    callback,
    options = {base: 'views', httpREsponse: true, interpolate: true},
  ) {
    if (typeof params === 'function') {
      callback = params;
      params = {};
    }
    interpolate(
      path.join(__dirname, '..', 'app', options.base),
      file,
      {...params, global: config.global},
      {'Content-Type': helpers.contentType(file)},
      callback,
      options,
    );
  },

  render(file, params, callback) {
    this.genericRender(file, params, callback, {
      base: 'views',
      httpREsponse: true,
      interpolate: true,
    });
  },
};
