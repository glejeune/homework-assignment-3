/*
 * @author: Gregoire Lejeune
 */

// Imports
const http = require('http');
const https = require('https');
const fs = require('fs');
const config = require('./app/config');
const router = require('./lib/router');
const logger = require('./lib/logger');

http.createServer(router).listen(config.httpPort, () => {
  logger.info(`The HTTP server is running on port ${config.httpPort}`);
});

// Create and start HTTPS server
const httpsServerOptions = {
  key: fs.readFileSync('./https/localhost.key'),
  cert: fs.readFileSync('./https/localhost.crt'),
};
https.createServer(httpsServerOptions, router).listen(config.httpsPort, () => {
  logger.info(`The HTTPS server is running on port ${config.httpsPort}`);
});
