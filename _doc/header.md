Homework Assignment #3 for [The Node.js Master Class](https://pirple.thinkific.com/courses/the-nodejs-master-class).

# <a name="assignment"></a> Assignment

It is time to build a simple frontend for the Pizza-Delivery API you created in Homework Assignment #2. Please create a web app that allows customers to:

1. Signup on the site
2. View all the items available to order
3. Fill up a shopping cart
4. Place an order (with <a href="https://stripe.com/docs/testing#cards">fake credit card credentials</a>), and receive an email receipt

This is an open-ended assignment. You can take any direction you'd like to go with it, as long as your project includes the requirements. It can include anything else you wish as well.

# <a name="configuration"></a> Configuration

Before running the server, you must edit the configuration in `lib/config.js`. In this file edit the values for keys :

* `environment.[staging, production].mailgun.domain` : Your Mailgun domain.
* `environment.[staging, production].mailgun.privateKey` : Your Mailgun private key.
* `environment.[staging, production].mailgun.from` : The `name` of the sender. Thus the sender email will be `mailgun.from`@`mailgun.domain`.
* `environment.[staging, production].stripe.secretKey` : Your Stripe private key.

# <a name="run"></a> Run

```
git clone https://gitlab.com/glejeune/homework-assignment-3
cd homework-assignment-3
node index.js
```

Then go to http://localhost:3000

# <a name="makefile"></a> Makefile

This project comes with a `Makefile` with the following rules:

* `default`: Start the server.
* `doc`: Generate the README.md file with API documentation.
* `clean`: Remove generated files (https certificats and keys) and application datas (`.data` directory)
