<a name="top"></a>
# Homework Assignment #3 v1

Homework Assignment #3 APIs

- [Cards](#cards)
	- [Delete the content of the card](#delete-the-content-of-the-card)
	- [Get all card items](#get-all-card-items)
	- [Add or update a menu item in the card](#add-or-update-a-menu-item-in-the-card)
	
- [MenuItems](#menuitems)
	- [Get menu items list](#get-menu-items-list)
	
- [Orders](#orders)
	- [Get all orders informations](#get-all-orders-informations)
	- [Create a new order](#create-a-new-order)
	
- [Ping](#ping)
	- [Get application status](#get-application-status)
	
- [Tokens](#tokens)
	- [Detele token](#detele-token)
	- [Get token informations](#get-token-informations)
	- [Request a new token](#request-a-new-token)
	- [Update token](#update-token)
	
- [Users](#users)
	- [Delete user](#delete-user)
	- [Get user informations](#get-user-informations)
	- [Create a new user](#create-a-new-user)
	- [Update a user](#update-a-user)
	


Homework Assignment #3 for [The Node.js Master Class](https://pirple.thinkific.com/courses/the-nodejs-master-class).

# <a name="assignment"></a> Assignment

It is time to build a simple frontend for the Pizza-Delivery API you created in Homework Assignment #2. Please create a web app that allows customers to:

1. Signup on the site
2. View all the items available to order
3. Fill up a shopping cart
4. Place an order (with <a href="https://stripe.com/docs/testing#cards">fake credit card credentials</a>), and receive an email receipt

This is an open-ended assignment. You can take any direction you'd like to go with it, as long as your project includes the requirements. It can include anything else you wish as well.

# <a name="configuration"></a> Configuration

Before running the server, you must edit the configuration in `lib/config.js`. In this file edit the values for keys :

* `environment.[staging, production].mailgun.domain` : Your Mailgun domain.
* `environment.[staging, production].mailgun.privateKey` : Your Mailgun private key.
* `environment.[staging, production].mailgun.from` : The `name` of the sender. Thus the sender email will be `mailgun.from`@`mailgun.domain`.
* `environment.[staging, production].stripe.secretKey` : Your Stripe private key.

# <a name="run"></a> Run

```
git clone https://gitlab.com/glejeune/homework-assignment-3
cd homework-assignment-3
node index.js
```

Then go to http://localhost:3000

# <a name="makefile"></a> Makefile

This project comes with a `Makefile` with the following rules:

* `default`: Start the server.
* `doc`: Generate the README.md file with API documentation.
* `clean`: Remove generated files (https certificats and keys) and application datas (`.data` directory)

# <a name='cards'></a> Cards

## <a name='delete-the-content-of-the-card'></a> Delete the content of the card
[Back to top](#top)



	DELETE /api/v1/cards

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| token | String | <p>User token</p>|


### Examples

Delete card item:

```
curl -v -X DELETE http://localhost:3000/api/v1/cards \
  -H "Token: abcdefjhij0123456789"
```


### Success Response

HTTP/1.1 204 No Content

```
HTTP/1.1 204 No Content
```


### Error Response

HTTP/1.1 401 Unauthorized

```
HTTP/1.1 401 Unauthorized
```
## <a name='get-all-card-items'></a> Get all card items
[Back to top](#top)



	GET /api/v1/cards

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| token | String | <p>User token</p>|


### Examples

Get menu items:

```
curl -v -X GET http://localhost:3000/api/v1/cards \
  -H "Token: abcdefjhij0123456789"
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
[
  {
    "id": "1",
    "quantity": 2
    "name": 'Veggie',
    "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
    "amount": 9.99,
    "currency": "USD"
  },
  {
    "id": "2",
    "quantity": 1,
    "name": "ExtravaganZZa",
    "description": "Loads of pepperoni, ham, savory Italian sausage, ground beef, fresh onions, fresh green peppers, fresh mushrooms and black olives with extra cheese.",
    "amount": 10.99,
    "currency": "USD"
  }
]
```


### Error Response

HTTP/1.1 401 Unauthorized

```
HTTP/1.1 401 Unauthorized
```
## <a name='add-or-update-a-menu-item-in-the-card'></a> Add or update a menu item in the card
[Back to top](#top)



	PUT /api/v1/cards

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| token | String | <p>User token</p>|




### Request body Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  menuItemId | String | <p>Menu item ID</p>|
|  quantity | Integer | <p>Quantity. Set to 0 to remove the item.</p>|
### Examples

Create a new card:

```
curl -v -X POST http://localhost:3000/api/v1/cards \
  -H "Token: abcdefjhij0123456789" \
  -d '{
    "menuItemId": "1",
    "quantity": 2
  }
]'
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
{
  "id": "1",
  "quantity": 2
  "name": 'Veggie',
  "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
  "amount": 9.99,
  "currency": "USD"
}
```


### Error Response

HTTP/1.1 400 Bad Request

```
HTTP/1.1 400 Bad Request
{
  "error": "Invalid or missing parameter"
}
```
# <a name='menuitems'></a> MenuItems

## <a name='get-menu-items-list'></a> Get menu items list
[Back to top](#top)



	GET /api/v1/menu-items

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| token | String | <p>User token</p>|


### Examples

Get menu items list:

```
curl -v -X GET http://localhost:3000/api/v1/menu-items \
  -H "Token: abcdefjhij0123456789"
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
[
  {
    "id": '1',
    "name": 'Veggie',
    "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
    "amount": 9.99,
    "currency": "USD"
  },
  ...
]
```


### Error Response

HTTP/1.1 401 Unauthorized

```
HTTP/1.1 401 Unauthorized
```
# <a name='orders'></a> Orders

## <a name='get-all-orders-informations'></a> Get all orders informations
[Back to top](#top)



	GET /api/v1/orders

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| token | String | <p>User token</p>|


### Examples

Get all orders:

```
curl -v -X POST http://localhost:3000/api/v1/tokens \
  -H "Token: abcdefjhij0123456789"
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
[
  {
    "id": "john.doe@example.com-1234567890987"
    "user": "john.doe@example.com",
    "amount": 30.97,
    "currency": "USD",
    "date": "09/08/2014, 2:35:56 AM",
    "content": [
      {
        "id": "1",
        "quantity": 2
        "name": 'Veggie',
        "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
        "amount": 9.99,
        "currency": "USD"
      },
      {
        "id": "2",
        "quantity": 1,
        "name": "ExtravaganZZa",
        "description": "Loads of pepperoni, ham, savory Italian sausage, ground beef, fresh onions, fresh green peppers, fresh mushrooms and black olives with extra cheese.",
        "amount": 10.99,
        "currency": "USD"
      }
    ]
  },
  {
    "id": "john.doe@example.com-1234567890123"
    "user": "john.doe@example.com",
    "amount": 9.99,
    "currency": "USD",
    "date": "09/09/2014, 2:35:56 AM",
    "content": [
      {
        "id": "1",
        "quantity": 1
        "name": 'Veggie',
        "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
        "amount": 9.99,
        "currency": "USD"
      }
    ]
  }
]
```


### Error Response

HTTP/1.1 401 Unauthorized

```
HTTP/1.1 401 Unauthorized
```
## <a name='create-a-new-order'></a> Create a new order
[Back to top](#top)



	POST /api/v1/orders

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| token | String | <p>User token</p>|


### Examples

Create a new order:

```
curl -v -X POST http://localhost:3000/api/v1/tokens \
  -H "Token: abcdefjhij0123456789"
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
{
  "id": "john.doe@example.com-1234567890987"
  "user": "john.doe@example.com",
  "amount": 30.97,
  "currency": "USD",
  "date": "09/08/2014, 2:35:56 AM",
  "content": [
    {
      "id": "1",
      "quantity": 2
      "name": 'Veggie',
      "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
      "amount": 9.99,
      "currency": "USD"
    },
    {
      "id": "2",
      "quantity": 1,
      "name": "ExtravaganZZa",
      "description": "Loads of pepperoni, ham, savory Italian sausage, ground beef, fresh onions, fresh green peppers, fresh mushrooms and black olives with extra cheese.",
      "amount": 10.99,
      "currency": "USD"
    }
  ]
}
```


### Error Response

HTTP/1.1 401 Unauthorized

```
HTTP/1.1 401 Unauthorized
```
# <a name='ping'></a> Ping

## <a name='get-application-status'></a> Get application status
[Back to top](#top)



	GET /ping



### Examples

Example usage:

```
curl -v -X GET http://localhost:3000/ping
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
```


# <a name='tokens'></a> Tokens

## <a name='detele-token'></a> Detele token
[Back to top](#top)



	DELETE /api/v1/tokens





### Query string Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  token | String | <p>User token</p>|
### Examples

Delete token:

```
curl -v -X DELETE http://localhost:3000/api/v1/tokens?token=abcdefjhij0123456789
```


### Success Response

HTTP/1.1 204 No Content

```
HTTP/1.1 204 No Content
```


### Error Response

HTTP/1.1 400 Bad Request

```
HTTP/1.1 400 Bad Request
{
  "error": "Invalid or missing parameters"
}
```
## <a name='get-token-informations'></a> Get token informations
[Back to top](#top)



	GET /api/v1/tokens





### Query string Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  token | String | <p>User token</p>|
### Examples

Get token informations:

```
curl -v -X GET http://localhost:3000/api/v1/tokens?token=abcdefjhij0123456789
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
{
  "email": "john.doe@example.com",
  "token": "abcdefjhij0123456789",
  "expires": 1234567890000
}
```


### Error Response

HTTP/1.1 400 Bad Request

```
HTTP/1.1 400 Bad Request
{
  "error": "Invalid or missing parameters"
}
```
## <a name='request-a-new-token'></a> Request a new token
[Back to top](#top)



	POST /api/v1/tokens





### Request body Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  email | String | <p>User email address</p>|
|  password | String | <p>User password</p>|
### Examples

Create a new token:

```
curl -v -X POST http://localhost:3000/api/v1/tokens -d '{
  "email": "john.doe@example.com",
  "password": "j0hnp4ssw0rd"
}'
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
{
  "email": "john.doe@example.com",
  "token": "abcdefjhij0123456789",
  "expires": 1234567890000
}
```


### Error Response

HTTP/1.1 400 Bad Request

```
HTTP/1.1 400 Bad Request
{
  "error": "Invalid username or password"
}
```
## <a name='update-token'></a> Update token
[Back to top](#top)



	PUT /api/v1/tokens





### Request body Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  token | String | <p>User token</p>|
|  extend | Boolean | <p>Expiration extension</p>|
### Examples

Extend token availability:

```
curl -v -X PUT http://localhost:3000/api/v1/tokens -d '{
  "token": "abcdefjhij0123456789",
  "extend": true
}'
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
{
  "email": "john.doe@example.com",
  "token": "abcdefjhij0123456789",
  "expires": 1234567890000
}
```


### Error Response

HTTP/1.1 400 Bad Request

```
HTTP/1.1 400 Bad Request
{
  "error": "Invalid or missing parameters"
}
```
# <a name='users'></a> Users

## <a name='delete-user'></a> Delete user
[Back to top](#top)



	DELETE /api/v1/users

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| token | String | <p>User token</p>|




### Query string Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  email | String | <p>User email</p>|
### Examples

Delete user:

```
curl -v -X DELETE http://localhost:3000/api/v1/users?email=john.doe@example.com \
  -H "Token: abcdefjhij0123456789"
```


### Success Response

HTTP/1.1 204 No Content

```
HTTP/1.1 204 No Content
```


### Error Response

HTTP/1.1 400 Bad Request

```
HTTP/1.1 400 Bad Request
{
  "error": "Missing required field"
}
```
## <a name='get-user-informations'></a> Get user informations
[Back to top](#top)



	GET /api/v1/users

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| token | String | <p>User token</p>|




### Query string Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  email | String | <p>User email</p>|
### Examples

Get user informations:

```
curl -v -X GET http://localhost:3000/api/v1/users?email=john.doe@example.com \
  -H "Token: abcdefjhij0123456789"
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
{
  "fullName": "John Doe",
  "email": "john.doe@example.com",
  "address": "8775 Hanover St. New City, NY 10956"
}
```


### Error Response

HTTP/1.1 400 Bad Request

```
HTTP/1.1 400 Bad Request
{
  "error": "Missing required field"
}
```
## <a name='create-a-new-user'></a> Create a new user
[Back to top](#top)



	POST /api/v1/users





### Request body Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  fullName | String | <p>User full name</p>|
|  email | String | <p>User email address</p>|
|  address | Sting | <p>User postal address</p>|
|  password | String | <p>User password</p>|
### Examples

Create a new user:

```
curl -v -X POST http://localhost:3000/api/v1/users -d '{
  "fullName": "John Doe",
  "email": "john.doe@example.com",
  "address": "8775 Hanover St. New City, NY 10956",
  "password": "j0hnp4ssw0rd"
}'
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
{
  "fullName": "John Doe",
  "email": "john.doe@example.com",
  "address": "8775 Hanover St. New City, NY 10956"
}
```


### Error Response

HTTP/1.1 400 Bad Request

```
HTTP/1.1 400 Bad Request
{
  "error": "A user with that email already exists"
}
```
## <a name='update-a-user'></a> Update a user
[Back to top](#top)



	PUT /api/v1/users

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| token | String | <p>User token</p>|




### Request body Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  email | String | <p>User email address</p>|
|  fullName | String | **optional**<p>New user full name</p>|
|  address | Sting | **optional**<p>New user postal address</p>|
|  password | String | **optional**<p>New user password</p>|
### Examples

Update user address:

```
curl -v -X PUT http://localhost:3000/api/v1/users \
  -H "Token: abcdefjhij0123456789" \
  -d '{
    "email": "john.doe@example.com",
    "address": "8775 Hanover St. New City, NY 10956",
  }'
```
Update user password and full name:

```
curl -v -X PUT http://localhost:3000/api/v1/users \
  -H "Token: abcdefjhij0123456789" \
  -d '{
    "email": "john.doe@example.com",
    "fullName": "John Doe Junior",
    "password": "j0hnjun10rp4ssw0rd"
  }'
```


### Success Response

HTTP/1.1 200 OK

```
HTTP/1.1 200 OK
{
  "fullName": "John Doe Junior",
  "email": "john.doe@example.com",
  "address": "8775 Hanover St. New City, NY 10956"
}
```


### Error Response

HTTP/1.1 400 Bad Request

```
HTTP/1.1 400 Bad Request
{
  "error": "Missing field to update"
}
```
