const https = require('https');
const querystring = require('querystring');
const helpers = require('../../lib/helpers');
const config = require('../config');

module.exports = {
  charge(amount, currency, description, source, callback) {
    const payload = querystring.stringify({
      amount: Math.ceil(amount * 100),
      currency: currency.toLowerCase(),
      description,
      source,
    });

    const requestDetails = {
      protocol: 'https:',
      hostname: 'api.stripe.com',
      method: 'POST',
      path: `/v1/charges`,
      auth: `${config.stripe.secretKey}:`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(payload),
      },
    };

    const req = https.request(requestDetails, res => {
      const status = res.statusCode;
      let data = [];

      res.on('data', chunk => {
        data.push(chunk);
      });
      res.on('end', () => {
        if (status === 200) {
          const charge = helpers.jsonToObject(
            Buffer.concat(data).toString('utf8'),
          );
          callback(false, charge.id);
        } else {
          callback(
            {error: `Stripe charge failed with status ${status}`},
            undefined,
          );
        }
      });
    });

    req.on('error', function(e) {
      callback(e);
    });

    req.write(payload);

    req.end();
  },
};
