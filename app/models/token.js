const fsStore = require('../../lib/fs-store');
const utils = require('../lib/utils');

module.exports = {
  create(user, callback) {
    const tokenId = utils.createRandomString(20);
    const tokenObject = {
      email: user.email,
      token: tokenId,
      expires: Date.now() + 1000 * 60 * 60,
    };

    fsStore.create('tokens', tokenId, tokenObject, err => {
      if (!err) {
        callback(false, tokenObject);
      } else {
        callback({error: 'Failed to create token'}, undefined);
      }
    });
  },

  verify(token, email, callback) {
    fsStore.read('tokens', token, (err, tokenObject) => {
      if (!err && tokenObject) {
        if (tokenObject.email === email && tokenObject.expires > Date.now()) {
          callback(true);
        } else {
          callback(false);
        }
      } else {
        callback(false);
      }
    });
  },

  findByID(tokenId, callback) {
    fsStore.read('tokens', tokenId, callback);
  },

  update(tokenId, tokenObject, callback) {
    fsStore.update('tokens', tokenId, tokenObject, callback);
  },

  delete(tokenId, callback) {
    fsStore.delete('tokens', tokenId, callback);
  },
};
