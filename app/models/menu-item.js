const MENU_DATA = {
  '1': {
    name: 'Veggie',
    description:
      'A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.',
    amount: 9.99,
    currency: 'USD',
    image: 'Veggie.jpg',
  },
  '2': {
    name: 'ExtravaganZZa',
    description:
      'Loads of pepperoni, ham, savory Italian sausage, ground beef, fresh onions, fresh green peppers, fresh mushrooms and black olives with extra cheese.',
    amount: 10.99,
    currency: 'USD',
    image: 'ExtravaganZZa.jpg',
  },
  '3': {
    name: 'Brooklyn Pizza',
    description:
      'Specifically engineered to be big, thin, and perfectly foldable.',
    amount: 10.99,
    currency: 'USD',
    image: 'BrooklynPizza.jpg',
  },
  '4': {
    name: 'BBQ Chicken Feast',
    description:
      'Smothered in flavorful BBQ sauce and loaded with chicken, bacon, onions, green peppers and cheddar cheese.',
    amount: 10.99,
    currency: 'USD',
    image: 'BBQChickenFeast.jpg',
  },
  '5': {
    name: 'Canadian Pizza',
    description:
      'Loads of pepperoni, fresh mushrooms, and smoked bacon, topped with an extra layer of premium mozzarella cheese.',
    amount: 12.99,
    currency: 'USD',
    image: 'CanadianPizza.jpg',
  },
  '6': {
    name: 'Deluxe',
    description:
      'A mouth-watering combination of pepperoni, savory Italian sausage, fresh green peppers, fresh mushrooms, fresh onions and cheese.',
    amount: 12.99,
    currency: 'USD',
    image: 'Deluxe.jpg',
  },
  '7': {
    name: 'Hawaiian',
    description:
      'Succulent pineapple and slices of ham topped with an extra layer of cheese.',
    amount: 9.99,
    currency: 'USD',
    image: 'Hawaiian.jpg',
  },
  '8': {
    name: 'MeatZZa',
    description:
      'Slice after slice of pepperoni, ham, savory Italian sausage and ground beef topped with an extra layer of cheese.',
    amount: 12.99,
    currency: 'USD',
    image: 'MeatZZa.jpg',
  },
};

module.exports = {
  find(callback) {
    const menuObjectLists = Object.keys(MENU_DATA).map(id => {
      return {...MENU_DATA[id], id};
    });
    callback(false, menuObjectLists);
  },

  findByID(id, callback) {
    if (MENU_DATA[id]) {
      callback(false, {...MENU_DATA[id], id});
    } else {
      callback({error: `Item ${id} does not exist in menu`}, undefined);
    }
  },
};
