const fsStore = require('../../lib/fs-store');

module.exports = {
  create(orderObject, callback) {
    fsStore.create('orders', orderObject.id, orderObject, err => {
      if (!err) {
        callback(false, orderObject);
      } else {
        callback({error: 'Failed to create order'}, undefined);
      }
    });
  },

  delete(orderId, callback) {
    fsStore.delete('orders', orderId, callback);
  },

  findByID(orderId, callback) {
    fsStore.read('orders', orderId, callback);
  },

  format(orderObject) {
    let response = `Dear customer,\n\nHere is the recap of your order ${
      orderObject.id
    } :\n\n`;
    response = orderObject.content.reduce(
      (acc, {name, quantity, amount, currency}) =>
        `${acc}${name}:\n  - Quantity: ${quantity}\n  - Unit price: ${currency}${amount}\n`,
      response,
    );
    return `${response}\nTotal: ${orderObject.currency}${
      orderObject.amount
    }\n\nThanks for your order!`;
  },
};
