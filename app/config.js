let environments = {};

environments.staging = {
  httpPort: 3000,
  httpsPort: 3001,
  envName: 'staging',
  hashingSecret: 'sup3rs3cr3t4h4sh1ngp4ss0rd1nst4g1ng',
  mailgun: {
    domain: 'sandboxdbXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.mailgun.org',
    privateKey: 'key-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    from: 'staging',
  },
  stripe: {
    secretKey: 'sk_test_XXXXXXXXXXXXXXXXXXXXXXXX',
  },
  global: {
    appName: 'Pizza Mama',
    compagnyName: 'The FooBar co.',
  },
};

environments.production = {
  httpPort: 5000,
  httpsPort: 5001,
  envName: 'production',
  hashingSecret: 'sup3rs3cr3t4h4sh1ngp4ss0rd1npr0duct10n',
  mailgun: {
    domain: 'sandboxdbXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.mailgun.org',
    privateKey: 'key-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    from: 'staging',
  },
  stripe: {
    secretKey: 'sk_test_XXXXXXXXXXXXXXXXXXXXXXXX',
  },
  global: {
    appName: 'Pizza Mama',
    compagnyName: 'The FooBar co.',
  },
};

const currentEnvironment =
  typeof process.env.NODE_ENV === 'string'
    ? process.env.NODE_ENV.toLowerCase()
    : '';

const environmentToExport =
  typeof environments[currentEnvironment] === 'object'
    ? environments[currentEnvironment]
    : environments.staging;

module.exports = environmentToExport;
