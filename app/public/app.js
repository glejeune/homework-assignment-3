const app = {
  sessionToken: false,

  setLoggedInClass() {
    const target = document.querySelector('body');
    if (app.sessionToken) {
      target.classList.add('loggedIn');
    } else {
      target.classList.remove('loggedIn');
    }
  },

  setSessionToken(token) {
    app.sessionToken = token;
    const tokenString = JSON.stringify(token);
    localStorage.setItem('token', tokenString);
    if (typeof token === 'object') {
      app.setLoggedInClass();
    } else {
      app.setLoggedInClass();
    }
  },

  getSessionToken() {
    const tokenString = localStorage.getItem('token');
    if (typeof tokenString === 'string') {
      try {
        const token = JSON.parse(tokenString);
        app.sessionToken = token;
        if (typeof token === 'object') {
          app.setLoggedInClass();
        } else {
          app.setLoggedInClass();
        }
      } catch (e) {
        app.sessionToken = false;
        app.setLoggedInClass();
      }
    }
  },

  redirectOnOnvalidRoute() {
    const REDIRECT_SIGNEDOUT = {
      '/settings': '/signin',
      '/card': '/signin',
    };
    const REDIRECT_SIGNEDIN = {
      '/signin': '/settings',
      '/signup': '/settings',
      '/signout': '/settings',
    };
    const newLocation = app.sessionToken
      ? REDIRECT_SIGNEDIN[window.location.pathname]
      : REDIRECT_SIGNEDOUT[window.location.pathname];
    if (newLocation) {
      window.location = newLocation;
    }
  },

  request(headers, path, method, queryStringObject, payload, callback) {
    // Set defaults
    headers = typeof headers === 'object' && headers !== null ? headers : {};
    path = typeof path === 'string' ? path : '/';
    method =
      typeof method === 'string' &&
      ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'].indexOf(method.toUpperCase()) >
        -1
        ? method.toUpperCase()
        : 'GET';
    queryStringObject =
      typeof queryStringObject === 'object' && queryStringObject !== null
        ? queryStringObject
        : {};
    payload = typeof payload === 'object' && payload !== null ? payload : {};
    callback = typeof callback === 'function' ? callback : false;

    // For each query string parameter sent, add it to the path
    const qs = Object.keys(queryStringObject)
      .map(key => `${key}=${queryStringObject[key]}`)
      .join('&');
    const requestUrl = `${path}?${qs}`;

    // Form the http request as a JSON type
    const xhr = new XMLHttpRequest();
    xhr.open(method, requestUrl, true);
    xhr.setRequestHeader('Content-type', 'application/json');

    // For each header sent, add it to the request
    Object.keys(headers).forEach(header => {
      xhr.setRequestHeader(header, headers[header]);
    });

    // If there is a current session token set, add that as a header
    if (app.sessionToken) {
      xhr.setRequestHeader('token', app.sessionToken.token);
    }

    // When the request comes back, handle the response
    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        const statusCode = xhr.status;
        const responseReturned = xhr.responseText;

        // Callback if requested
        if (callback) {
          try {
            const parsedResponse = JSON.parse(responseReturned);
            callback(statusCode, parsedResponse);
          } catch (e) {
            callback(statusCode, false);
          }
        }
      }
    };

    // Send the payload as JSON
    const payloadString = JSON.stringify(payload);
    xhr.send(payloadString);
  },

  signOutUser(redirectUser) {
    redirectUser = typeof redirectUser === 'boolean' ? redirectUser : true;

    const tokenId =
      typeof app.sessionToken.token === 'string'
        ? app.sessionToken.token
        : false;

    const queryStringObject = {
      token: tokenId,
    };
    app.request(
      undefined,
      '/api/v1/tokens',
      'DELETE',
      queryStringObject,
      undefined,
      function(statusCode, responsePayload) {
        app.setSessionToken();

        if (redirectUser) {
          window.location = '/signout';
        }
      },
    );
  },

  formResponseProcessor(formId, requestPayload, responsePayload) {
    if (formId === 'accountCreate') {
      // Take the phone and password, and use it to log the user in
      const newPayload = {
        email: requestPayload.email,
        password: requestPayload.password,
      };

      app.request(
        undefined,
        '/api/v1/tokens',
        'POST',
        undefined,
        newPayload,
        function(newStatusCode, newResponsePayload) {
          // Display an error on the form if needed
          if (newStatusCode !== 200) {
            // Set the formError field with the error text
            document.querySelector('#' + formId + ' .formError').innerHTML =
              'Sorry, an error has occured. Please try again.';

            // Show (unhide) the form error field on the form
            document.querySelector('#' + formId + ' .formError').style.display =
              'block';
          } else {
            // If successful, set the token and redirect the user
            app.setSessionToken(newResponsePayload);
            window.location = '/';
          }
        },
      );
    }

    if (formId === 'sessionCreate') {
      app.setSessionToken(responsePayload);
      window.location = '/';
    }

    const formsWithSuccessMessages = ['accountEdit1', 'accountEdit2'];
    if (formsWithSuccessMessages.indexOf(formId) > -1) {
      document.querySelector('#' + formId + ' .formSuccess').style.display =
        'block';
    }

    if (formId === 'accountEdit3') {
      app.signOutUser(false);
      window.location = '/deleted';
    }

    if (formId === 'order') {
      window.location = '/ordered';
    }
  },

  bindForms() {
    if (document.querySelector('form')) {
      const allForms = document.querySelectorAll('form');
      allForms.forEach(currentForm => {
        currentForm.addEventListener('submit', function(e) {
          // Stop it from submitting
          e.preventDefault();
          const formId = this.id;
          const path = this.action;
          let method = this.method.toUpperCase();

          // clean form messages
          document.querySelector('#' + formId + ' .formError').style.display =
            'none';

          if (document.querySelector('#' + formId + ' .formSuccess')) {
            document.querySelector(
              '#' + formId + ' .formSuccess',
            ).style.display = 'none';
          }

          // Turn the inputs into a payload
          let payload = {};
          const elements = this.elements;
          for (let i = 0; i < elements.length; i++) {
            if (elements[i].type !== 'submit') {
              // Determine class of element and set value accordingly
              const classOfElement =
                typeof elements[i].classList.value === 'string' &&
                elements[i].classList.value.length > 0
                  ? elements[i].classList.value
                  : '';
              const valueOfElement =
                classOfElement.indexOf('intval') === -1
                  ? elements[i].value
                  : parseInt(elements[i].value);
              const nameOfElement = elements[i].name;
              if (nameOfElement === '_method') {
                method = valueOfElement;
              } else {
                payload[nameOfElement] = valueOfElement;
              }
            }
          }

          // If the method is DELETE, the payload should be a queryStringObject instead
          const queryStringObject = method === 'DELETE' ? payload : {};

          // Call the API
          app.request(
            undefined,
            path,
            method,
            queryStringObject,
            payload,
            function(statusCode, responsePayload = {}) {
              // Display an error on the form if needed
              if (statusCode !== 200 && statusCode !== 204) {
                if (statusCode === 403) {
                  // log the user out
                  app.signOutUser();
                } else {
                  // Try to get the error from the api, or set a default error message
                  const error =
                    typeof responsePayload.error === 'string'
                      ? responsePayload.error
                      : 'An error has occured, please try again';

                  // Set the formError field with the error text
                  document.querySelector(
                    '#' + formId + ' .formError',
                  ).innerHTML = error;

                  // Show (unhide) the form error field on the form
                  document.querySelector(
                    '#' + formId + ' .formError',
                  ).style.display = 'block';
                }
              } else {
                // If successful, send to form response processor
                app.formResponseProcessor(formId, payload, responsePayload);
              }
            },
          );
        });
      });
    }
  },

  bindSignOutMenu() {
    document
      .getElementById('signOutMenu')
      .addEventListener('click', function(e) {
        e.preventDefault();
        app.signOutUser();
      });
  },

  addToChart(menuItemId, menuItemName) {
    app.request(
      undefined,
      '/api/v1/cards',
      'PUT',
      undefined,
      {menuItemId: `${menuItemId}`, quantity: 1},
      function(newStatusCode, newResponsePayload) {
        let element;
        if (newStatusCode !== 200) {
          element = document.getElementById('error');
          element.innerHTML = `ARG ! Something went wrong and we could not add a delicious ${menuItemName} in your card! We are so, so, sorry ! Please, try again !`;
        } else {
          element = document.getElementById('message');
          element.innerHTML = `You just add a delicious ${menuItemName} in your card! You will love it !`;
        }
        if (app.timer) {
          clearInterval(app.timer);
        }
        element.style.opacity = 1;
        element.style.display = 'block';
        setTimeout(() => {
          let op = 1;
          app.timer = setInterval(function() {
            if (op <= 0.1) {
              clearInterval(app.timer);
              element.style.display = 'none';
            }
            element.style.opacity = op;
            element.style.filter = 'alpha(opacity=' + op * 100 + ')';
            op -= op * 0.1;
          }, 50);
        }, 2000);
      },
    );
  },

  updateCardItem(id, amount, currency) {
    const newQuantity = parseInt(
      document.getElementById(`cardItemNewQuantity${id}`).value,
    );

    const initialQuantity = parseInt(
      document.getElementById(`cardItemInitialQuantity${id}`).value,
    );
    if (newQuantity !== initialQuantity) {
      app.request(
        undefined,
        '/api/v1/cards',
        'PUT',
        undefined,
        {menuItemId: `${id}`, quantity: newQuantity},
        function(newStatusCode, newResponsePayload) {
          let element;
          if (newStatusCode !== 201 && newStatusCode !== 200) {
            element = document.getElementById('error');
            element.innerHTML = `ARG ! Something went wrong and we could not update the card! We are so, so, sorry ! Please, try again !`;
          } else {
            if (newQuantity === 0) {
              document.getElementById(`cardRawItem${id}`).remove();
            }

            document.getElementById(
              `cardItemInitialQuantity${id}`,
            ).value = newQuantity;
            document.getElementById(
              `cardRawPrice${id}`,
            ).innerHTML = `${currency} ${amount * newQuantity}`;
            element = document.getElementById('message');
            element.innerHTML = `Card updated!`;
          }
          if (app.timer) {
            clearInterval(app.timer);
          }
          element.style.opacity = 1;
          element.style.display = 'block';
          setTimeout(() => {
            let op = 1;
            app.timer = setInterval(function() {
              if (op <= 0.1) {
                clearInterval(app.timer);
                element.style.display = 'none';
              }
              element.style.opacity = op;
              element.style.filter = 'alpha(opacity=' + op * 100 + ')';
              op -= op * 0.1;
            }, 50);
          }, 2000);
        },
      );
    }
  },

  loadProductList() {
    if (app.sessionToken) {
      app.request(
        undefined,
        '/api/v1/menu-items',
        'GET',
        undefined,
        undefined,
        (statusCode, responsePayload) => {
          if (statusCode === 200) {
            const productList = document.getElementById('products');
            for (let i = 0; i < responsePayload.length; i++) {
              productList.innerHTML += ` 
<div class="product-card">
  <div class="product-image">
    <img src="/${responsePayload[i].image || 'pizza.svg'}" />
    <div class="overlay"><p>${responsePayload[i].description}<p></div>
  </div>
  <div class="product-info">
    <h5>${responsePayload[i].name}</h5>
    <h6>${responsePayload[i].currency} ${responsePayload[i].amount}</h6>
  </div>
  <input 
    class="cta" 
    type="button" 
    value="Add to chart" 
    onclick="app.addToChart(${responsePayload[i].id}, '${
                responsePayload[i].name
              }')"/> 
</div>`;
            }
          } else {
            app.signOutUser();
          }
        },
      );
    } else {
      document.getElementById('menuNotLoaded').style.display = 'block';
    }
  },

  loadUserSettings() {
    const email =
      typeof app.sessionToken.email === 'string'
        ? app.sessionToken.email
        : false;
    if (email) {
      const queryStringObject = {email};
      app.request(
        undefined,
        '/api/v1/users',
        'GET',
        queryStringObject,
        undefined,
        function(statusCode, responsePayload) {
          if (statusCode === 200) {
            document.querySelector('#accountEdit1 .displayEmailInput').value =
              responsePayload.email;
            document.querySelector('#accountEdit1 .fullNameInput').value =
              responsePayload.fullName;
            document.querySelector('#accountEdit1 .addressInput').value =
              responsePayload.address;

            const hiddenEmailInputs = document.querySelectorAll(
              'input.hiddenEmailInput',
            );
            for (let i = 0; i < hiddenEmailInputs.length; i++) {
              hiddenEmailInputs[i].value = responsePayload.email;
            }
          } else {
            app.signOutUser();
          }
        },
      );
    } else {
      app.signOutUser();
    }
  },

  loadOrders() {
    if (app.sessionToken) {
      app.request(
        undefined,
        '/api/v1/orders',
        'GET',
        undefined,
        undefined,
        (statusCode, responsePayload) => {
          if (statusCode === 200 && responsePayload.length > 0) {
            const table = document.getElementById('orderList');
            document.getElementById('previousOrders').style.display = 'block';
            responsePayload.forEach(order => {
              const tr = table.insertRow(-1);
              const td0 = tr.insertCell(0);
              const td1 = tr.insertCell(1);
              const td2 = tr.insertCell(2);
              const td3 = tr.insertCell(3);
              td0.innerHTML = order.date;
              td1.innerHTML = order.id;
              td2.innerHTML = `<ul>${order.content.reduce(
                (acc, {name, quantity}) =>
                  `${acc}<li>${name}: ${quantity}</li>`,
                '',
              )}</ul>`;
              td3.innerHTML = `${order.currency} ${order.amount}`;
            });
          }
        },
      );
    }
  },

  loadShoppingCard() {
    const email =
      typeof app.sessionToken.email === 'string'
        ? app.sessionToken.email
        : false;
    if (email) {
      app.request(
        undefined,
        '/api/v1/cards',
        'GET',
        undefined,
        undefined,
        function(statusCode, responsePayload) {
          if (statusCode === 200) {
            if (responsePayload.length > 0) {
              const table = document.getElementById('cardList');
              document.getElementById('emptyCard').style.display = 'none';
              document.getElementById('card').style.display = 'block';
              responsePayload.forEach(item => {
                const tr = table.insertRow(-1);
                tr.classList.add('checkRow');
                tr.setAttribute(`id`, `cardRawItem${item.id}`);
                const td0 = tr.insertCell(0);
                const td1 = tr.insertCell(1);
                const td2 = tr.insertCell(2);
                td2.setAttribute(`id`, `cardRawPrice${item.id}`);
                const td3 = tr.insertCell(3);
                td0.innerHTML = `<img src="/${item.image}" height="50px"/>`;
                td1.innerHTML = item.name;
                td2.innerHTML = `${item.currency} ${item.amount *
                  item.quantity}`;
                td3.innerHTML = `<form><input id="cardItemNewQuantity${
                  item.id
                }" class="small" type="text" value="${
                  item.quantity
                }"><input id="cardItemInitialQuantity${
                  item.id
                }" type="hidden" value="${
                  item.quantity
                }"><input class="cta small" type="button" value="Update" onclick="app.updateCardItem('${
                  item.id
                }', ${item.amount}, '${item.currency}')"></form>`;
              });
            } else {
              document.getElementById('emptyCard').style.display = 'block';
              document.getElementById('card').style.display = 'none';
            }
          } else {
            app.signOutUser();
          }
        },
      );
    } else {
      app.signOutUser();
    }
  },

  loadDataOnPage() {
    if (document.querySelector('#products')) {
      app.loadProductList();
    }
    if (document.querySelector('#userSettings')) {
      app.loadUserSettings();
      app.loadOrders();
    }
    if (document.querySelector('#shoppingCard')) {
      app.loadShoppingCard();
    }
  },

  init() {
    app.getSessionToken();
    app.redirectOnOnvalidRoute();
    app.bindForms();
    app.bindSignOutMenu();
    app.loadDataOnPage();
  },
};

window.onload = function() {
  app.init();
};
