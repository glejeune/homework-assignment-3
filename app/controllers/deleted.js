const controller = require('../../lib/controller');
const template = require('../../lib/template');

module.exports = controller('/deleted', {
  get(data, callback) {
    template.render(
      'deleted.html',
      {head: {title: 'Account deleted'}},
      callback,
    );
  },
});
