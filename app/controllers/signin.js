const controller = require('../../lib/controller');
const template = require('../../lib/template');

module.exports = controller('/signin', {
  get(data, callback) {
    template.render('signin.html', {head: {title: 'Sign in'}}, callback);
  },
});
