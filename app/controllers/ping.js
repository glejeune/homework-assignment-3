const controller = require('../../lib/controller');

module.exports = controller('/ping', {
  /**
   * @api {get} /ping Get application status
   * @apiName GetPing
   * @apiGroup Ping
   *
   * @apiExample {curl} Example usage:
   *     curl -v -X GET http://localhost:3000/ping
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   */
  get(data, callback) {
    callback(200);
  },
});
