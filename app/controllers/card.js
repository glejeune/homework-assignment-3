const controller = require('../../lib/controller');
const template = require('../../lib/template');

module.exports = controller('/card', {
  get(data, callback) {
    template.render('card.html', {head: {title: 'Card'}}, callback);
  },
});
