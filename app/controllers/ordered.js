const controller = require('../../lib/controller');
const template = require('../../lib/template');

module.exports = controller('/ordered', {
  get(data, callback) {
    template.render('ordered.html', {head: {title: 'Ordered'}}, callback);
  },
});
