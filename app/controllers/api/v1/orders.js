const controller = require('../../../../lib/controller');
const token = require('../../../models/token');
const user = require('../../../models/user');
const order = require('../../../models/order');
const menuItem = require('../../../models/menu-item');
const stripe = require('../../../lib/stripe');
const mailgun = require('../../../lib/mailgun');

module.exports = controller('/api/v1/orders', {
  /**
   * @api {post} /api/v1/orders Create a new order
   * @apiName PostOrders
   * @apiGroup Orders
   *
   * @apiHeader {String} token User token
   *
   * @apiExample {curl} Create a new order:
   *     curl -v -X POST http://localhost:3000/api/v1/tokens \
   *       -H "Token: abcdefjhij0123456789"
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     {
   *       "id": "john.doe@example.com-1234567890987"
   *       "user": "john.doe@example.com",
   *       "amount": 30.97,
   *       "currency": "USD",
   *       "date": "09/08/2014, 2:35:56 AM",
   *       "content": [
   *         {
   *           "id": "1",
   *           "quantity": 2
   *           "name": 'Veggie',
   *           "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
   *           "amount": 9.99,
   *           "currency": "USD"
   *         },
   *         {
   *           "id": "2",
   *           "quantity": 1,
   *           "name": "ExtravaganZZa",
   *           "description": "Loads of pepperoni, ham, savory Italian sausage, ground beef, fresh onions, fresh green peppers, fresh mushrooms and black olives with extra cheese.",
   *           "amount": 10.99,
   *           "currency": "USD"
   *         }
   *       ]
   *     }
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 401 Unauthorized
   */
  post(data, callback) {
    const tokenId =
      typeof data.headers.token === 'string' ? data.headers.token : false;

    user.findByTokenId(tokenId, (err, userObject) => {
      if (!err && userObject) {
        if (userObject.card && Object.keys(userObject.card).length > 0) {
          const orderId = `${userObject.email}-${Date.now}`;
          const currency = 'USD';

          menuItem.find((err, menuItemList) => {
            if (!err && menuItemList) {
              const [amount, orderContent] = menuItemList.reduce(
                ([amount, orderContent], menuItemObject) => {
                  const quantity = userObject.card[menuItemObject.id];
                  if (quantity) {
                    amount += menuItemObject.amount * quantity;
                    orderContent.push({...menuItemObject, quantity});
                  }
                  return [amount, orderContent];
                },
                [0, []],
              );

              stripe.charge(
                amount,
                currency,
                orderId,
                'tok_visa',
                (err, id) => {
                  if (!err) {
                    const orderObject = {
                      id,
                      user: userObject.email,
                      amount,
                      currency: 'USD',
                      date: new Date().toLocaleString(),
                      content: orderContent,
                    };
                    order.create(orderObject, (err, _orderObject) => {
                      if (!err && _orderObject) {
                        const userOrders =
                          typeof userObject.orders === 'object' &&
                          userObject.orders instanceof Array
                            ? userObject.orders
                            : [];
                        userObject.orders = userOrders;
                        userObject.orders.push(orderObject.id);
                        userObject.card = [];
                        user.update(userObject.email, userObject, err => {
                          if (!err) {
                            mailgun.send(
                              userObject.email,
                              `Order ${orderObject.id}`,
                              order.format(orderObject),
                              err => {
                                if (!err) {
                                  callback(200, orderObject);
                                } else {
                                  callback(500, err);
                                }
                              },
                            );
                          } else {
                            callback(500, {error: 'Could not update user'});
                          }
                        });
                      } else {
                        callback(500, {error: 'Failed to create order'});
                      }
                    });
                  } else {
                    callback(500, {error: 'Failed to create stripe order'});
                  }
                },
              );
            } else {
              callback(500, {error: 'Failed to load menu'});
            }
          });
        } else {
          callback(400, {error: 'Card empty'});
        }
      } else {
        callback(401);
      }
    });
  },

  /**
   * @api {get} /api/v1/orders Get all orders informations
   * @apiName GetOrders
   * @apiGroup Orders
   *
   * @apiHeader {String} token User token
   *
   * @apiExample {curl} Get all orders:
   *     curl -v -X POST http://localhost:3000/api/v1/tokens \
   *       -H "Token: abcdefjhij0123456789"
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     [
   *       {
   *         "id": "john.doe@example.com-1234567890987"
   *         "user": "john.doe@example.com",
   *         "amount": 30.97,
   *         "currency": "USD",
   *         "date": "09/08/2014, 2:35:56 AM",
   *         "content": [
   *           {
   *             "id": "1",
   *             "quantity": 2
   *             "name": 'Veggie',
   *             "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
   *             "amount": 9.99,
   *             "currency": "USD"
   *           },
   *           {
   *             "id": "2",
   *             "quantity": 1,
   *             "name": "ExtravaganZZa",
   *             "description": "Loads of pepperoni, ham, savory Italian sausage, ground beef, fresh onions, fresh green peppers, fresh mushrooms and black olives with extra cheese.",
   *             "amount": 10.99,
   *             "currency": "USD"
   *           }
   *         ]
   *       },
   *       {
   *         "id": "john.doe@example.com-1234567890123"
   *         "user": "john.doe@example.com",
   *         "amount": 9.99,
   *         "currency": "USD",
   *         "date": "09/09/2014, 2:35:56 AM",
   *         "content": [
   *           {
   *             "id": "1",
   *             "quantity": 1
   *             "name": 'Veggie',
   *             "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
   *             "amount": 9.99,
   *             "currency": "USD"
   *           }
   *         ]
   *       }
   *     ]
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 401 Unauthorized
   */
  get(data, callback) {
    const tokenId =
      typeof data.headers.token === 'string' ? data.headers.token : false;

    user.findByTokenId(tokenId, (err, userObject) => {
      if (!err && userObject) {
        const userOrders =
          typeof userObject.orders === 'object' &&
          userObject.orders instanceof Array
            ? userObject.orders
            : [];
        Promise.all(
          userObject.orders.map(
            orderId =>
              new Promise((resolve, reject) => {
                order.findByID(orderId, (err, orderObject) => {
                  if (!err && orderObject) {
                    resolve(orderObject);
                  } else {
                    reject(err);
                  }
                });
              }),
          ),
        )
          .then(values => {
            callback(200, values);
          })
          .catch(err => {
            callback(500, {error: 'Failed to retrieve orders'});
          });
      } else {
        callback(401);
      }
    });
  },
});
