const controller = require('../../../../lib/controller');
const token = require('../../../models/token');
const user = require('../../../models/user');

module.exports = controller('api/v1/tokens', {
  /**
   * @api {post} /api/v1/tokens Request a new token
   * @apiName PostTokens
   * @apiGroup Tokens
   *
   * @apiParam (Request body) {String} email User email address
   * @apiParam (Request body) {String} password User password
   *
   * @apiExample {curl} Create a new token:
   *     curl -v -X POST http://localhost:3000/api/v1/tokens -d '{
   *       "email": "john.doe@example.com",
   *       "password": "j0hnp4ssw0rd"
   *     }'
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     {
   *       "email": "john.doe@example.com",
   *       "token": "abcdefjhij0123456789",
   *       "expires": 1234567890000
   *     }
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 400 Bad Request
   *     {
   *       "error": "Invalid username or password"
   *     }
   */
  post(data, callback) {
    const email =
      typeof data.payload.email === 'string' &&
      data.payload.email.trim().length > 0
        ? data.payload.email.trim()
        : false;
    const password =
      typeof data.payload.password === 'string' &&
      data.payload.password.trim().length > 0
        ? data.payload.password.trim()
        : false;

    if (email && password) {
      user.login(email, password, (err, userObject) => {
        if (!err && userObject) {
          token.create(userObject, (err, tokenObject) => {
            if (!err && tokenObject) {
              const userTokens =
                typeof userObject.tokens === 'object' &&
                userObject.tokens instanceof Array
                  ? userObject.tokens
                  : [];
              userObject.tokens = userTokens;
              userObject.tokens.push(tokenObject.token);
              user.update(userObject.email, userObject, err => {
                if (!err) {
                  callback(200, tokenObject);
                } else {
                  callback(500, {error: 'Could not update user'});
                }
              });
            } else {
              callback(500, {error: 'Failed to create token'});
            }
          });
        } else {
          callback(400, {error: 'Invalid username or password'});
        }
      });
    } else {
      callback(400, {error: 'Invalid or missing parameters'});
    }
  },

  /**
   * @api {get} /api/v1/tokens Get token informations
   * @apiName GetTokens
   * @apiGroup Tokens
   *
   * @apiParam (Query string) {String} token User token
   *
   * @apiExample {curl} Get token informations:
   *     curl -v -X GET http://localhost:3000/api/v1/tokens?token=abcdefjhij0123456789
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     {
   *       "email": "john.doe@example.com",
   *       "token": "abcdefjhij0123456789",
   *       "expires": 1234567890000
   *     }
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 400 Bad Request
   *     {
   *       "error": "Invalid or missing parameters"
   *     }
   */
  get(data, callback) {
    const tokenId =
      typeof data.queryStringObject.token === 'string' &&
      data.queryStringObject.token.trim().length === 20
        ? data.queryStringObject.token.trim()
        : false;
    if (tokenId) {
      token.findByID(tokenId, (err, tokenObject) => {
        if (!err && tokenObject) {
          callback(200, tokenObject);
        } else {
          callback(404);
        }
      });
    } else {
      callback(400, {error: 'Invalid or missing parameter'});
    }
  },

  /**
   * @api {put} /api/v1/tokens Update token
   * @apiName PutTokens
   * @apiGroup Tokens
   *
   * @apiParam (Request body) {String} token User token
   * @apiParam (Request body) {Boolean} extend Expiration extension
   *
   * @apiExample {curl} Extend token availability:
   *     curl -v -X PUT http://localhost:3000/api/v1/tokens -d '{
   *       "token": "abcdefjhij0123456789",
   *       "extend": true
   *     }'
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     {
   *       "email": "john.doe@example.com",
   *       "token": "abcdefjhij0123456789",
   *       "expires": 1234567890000
   *     }
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 400 Bad Request
   *     {
   *       "error": "Invalid or missing parameters"
   *     }
   */
  put(data, callback) {
    const tokenId =
      typeof data.payload.token === 'string' &&
      data.payload.token.trim().length === 20
        ? data.payload.token.trim()
        : false;
    const extend =
      typeof data.payload.extend === 'boolean' && data.payload.extend === true
        ? true
        : false;

    if (tokenId && extend) {
      token.findByID(tokenId, (err, tokenObject) => {
        if (!err && tokenObject) {
          if (tokenObject.expires > Date.now()) {
            tokenObject.expires = Date.now() + 1000 * 60 * 60;
            token.update(tokenId, tokenObject, err => {
              if (!err) {
                callback(200, tokenObject);
              } else {
                callback(500, {
                  error: 'Could not update the token',
                });
              }
            });
          } else {
            token.delete(tokenId, _err => {
              callback(400, {
                error: 'The token has already expired, and cannot be extended.',
              });
            });
          }
        } else {
          callback(400, {error: 'Invalid token'});
        }
      });
    } else {
      callback(400, {
        error: 'Invalid or missing parameters',
      });
    }
  },

  /**
   * @api {delete} /api/v1/tokens Detele token
   * @apiName DeleteTokens
   * @apiGroup Tokens
   *
   * @apiParam (Query string) {String} token User token
   *
   * @apiExample {curl} Delete token:
   *     curl -v -X DELETE http://localhost:3000/api/v1/tokens?token=abcdefjhij0123456789
   *
   * @apiSuccessExample
   *     HTTP/1.1 204 No Content
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 400 Bad Request
   *     {
   *       "error": "Invalid or missing parameters"
   *     }
   */
  delete(data, callback) {
    const tokenId =
      typeof data.queryStringObject.token === 'string' &&
      data.queryStringObject.token.trim().length === 20
        ? data.queryStringObject.token.trim()
        : false;

    if (tokenId) {
      token.findByID(tokenId, (err, tokenObject) => {
        if (!err && tokenObject) {
          token.delete(tokenId, err => {
            if (!err) {
              callback(204);
            } else {
              callback(500, {error: 'Could not delete the specified token'});
            }
          });
        } else {
          callback(400, {error: 'Could not find the specified token'});
        }
      });
    } else {
      callback(400, {error: 'Missing or invalid parameters'});
    }
  },
});
