const controller = require('../../../../lib/controller');
const utils = require('../../../lib/utils');
const token = require('../../../models/token');
const user = require('../../../models/user');

module.exports = controller('api/v1/users', {
  /**
   * @api {post} /api/v1/users Create a new user
   * @apiName PostUsers
   * @apiGroup Users
   *
   * @apiParam (Request body) {String} fullName User full name
   * @apiParam (Request body) {String} email User email address
   * @apiParam (Request body) {Sting} address User postal address
   * @apiParam (Request body) {String} password User password
   *
   * @apiExample {curl} Create a new user:
   *     curl -v -X POST http://localhost:3000/api/v1/users -d '{
   *       "fullName": "John Doe",
   *       "email": "john.doe@example.com",
   *       "address": "8775 Hanover St. New City, NY 10956",
   *       "password": "j0hnp4ssw0rd"
   *     }'
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     {
   *       "fullName": "John Doe",
   *       "email": "john.doe@example.com",
   *       "address": "8775 Hanover St. New City, NY 10956"
   *     }
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 400 Bad Request
   *     {
   *       "error": "A user with that email already exists"
   *     }
   */
  post(data, callback) {
    const fullName =
      typeof data.payload.fullName === 'string' &&
      data.payload.fullName.trim().length > 0
        ? data.payload.fullName.trim()
        : false;
    const email =
      typeof data.payload.email === 'string' &&
      data.payload.email.trim().length > 0
        ? data.payload.email.trim()
        : false;
    const address =
      typeof data.payload.address === 'string' &&
      data.payload.address.trim().length > 0
        ? data.payload.address.trim()
        : false;
    const password =
      typeof data.payload.password === 'string' &&
      data.payload.password.trim().length > 0
        ? data.payload.password.trim()
        : false;

    if (fullName && email && address && password) {
      user.create({fullName, email, password, address}, (err, userObject) => {
        if (!err) {
          delete userObject.hashedPassword;
          callback(200, userObject);
        } else {
          callback(500, err);
        }
      });
    } else {
      callback(400, {error: 'Invalid or missing parameters'});
    }
  },

  /**
   * @api {get} /api/v1/users Get user informations
   * @apiName GetUsers
   * @apiGroup Users
   *
   * @apiParam (Query string) {String} email User email
   * @apiHeader {String} token User token
   *
   * @apiExample {curl} Get user informations:
   *     curl -v -X GET http://localhost:3000/api/v1/users?email=john.doe@example.com \
   *       -H "Token: abcdefjhij0123456789"
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     {
   *       "fullName": "John Doe",
   *       "email": "john.doe@example.com",
   *       "address": "8775 Hanover St. New City, NY 10956"
   *     }
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 400 Bad Request
   *     {
   *       "error": "Missing required field"
   *     }
   */
  get(data, callback) {
    const email =
      typeof data.queryStringObject.email === 'string' &&
      data.queryStringObject.email.trim().length > 0
        ? data.queryStringObject.email.trim()
        : false;

    const tokenId =
      typeof data.headers.token === 'string' ? data.headers.token : false;

    if (email && tokenId) {
      token.verify(tokenId, email, tokenIsValid => {
        if (tokenIsValid) {
          user.findByEmail(email, (err, userObject) => {
            if (!err && userObject) {
              delete userObject.hashedPassword;
              delete userObject.tokens;
              delete userObject.menuItems;
              callback(200, userObject);
            } else {
              callback(404);
            }
          });
        } else {
          callback(401);
        }
      });
    } else {
      callback(400, {error: 'Missing required field'});
    }
  },

  /**
   * @api {put} /api/v1/users Update a user
   * @apiName PutUsers
   * @apiGroup Users
   *
   * @apiParam (Request body) {String} email User email address
   * @apiParam (Request body) {String} [fullName] New user full name
   * @apiParam (Request body) {Sting} [address] New user postal address
   * @apiParam (Request body) {String} [password] New user password
   * @apiHeader {String} token User token
   *
   * @apiExample {curl} Update user address:
   *     curl -v -X PUT http://localhost:3000/api/v1/users \
   *       -H "Token: abcdefjhij0123456789" \
   *       -d '{
   *         "email": "john.doe@example.com",
   *         "address": "8775 Hanover St. New City, NY 10956",
   *       }'
   *
   * @apiExample {curl} Update user password and full name:
   *     curl -v -X PUT http://localhost:3000/api/v1/users \
   *       -H "Token: abcdefjhij0123456789" \
   *       -d '{
   *         "email": "john.doe@example.com",
   *         "fullName": "John Doe Junior",
   *         "password": "j0hnjun10rp4ssw0rd"
   *       }'
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     {
   *       "fullName": "John Doe Junior",
   *       "email": "john.doe@example.com",
   *       "address": "8775 Hanover St. New City, NY 10956"
   *     }
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 400 Bad Request
   *     {
   *       "error": "Missing field to update"
   *     }
   */
  put(data, callback) {
    const email =
      typeof data.payload.email === 'string'
        ? data.payload.email.trim()
        : false;

    const fullName =
      typeof data.payload.fullName === 'string' &&
      data.payload.fullName.trim().length > 0
        ? data.payload.fullName.trim()
        : false;
    const address =
      typeof data.payload.address === 'string' &&
      data.payload.address.trim().length > 0
        ? data.payload.address.trim()
        : false;
    const password =
      typeof data.payload.password === 'string' &&
      data.payload.password.trim().length > 0
        ? data.payload.password.trim()
        : false;

    const tokenId =
      typeof data.headers.token === 'string' ? data.headers.token : false;

    if (email) {
      if (fullName || address || password) {
        token.verify(tokenId, email, tokenIsValid => {
          if (tokenIsValid) {
            user.findByEmail(email, (err, userObject) => {
              if (!err && userObject) {
                if (fullName) {
                  userObject.fullName = fullName;
                }
                if (address) {
                  userObject.address = address;
                }
                if (password) {
                  userObject.hashedPassword = utils.hash(password);
                }
                user.update(email, userObject, err => {
                  if (!err) {
                    delete userObject.hashedPassword;
                    callback(200, userObject);
                  } else {
                    callback(500, {error: 'Could not update user'});
                  }
                });
              } else {
                callback(400, {error: 'Specified user does not exist'});
              }
            });
          } else {
            callback(401);
          }
        });
      } else {
        callback(400, {error: 'Missing fields to update'});
      }
    } else {
      callback(400, {error: 'Missing required field'});
    }
  },

  /**
   * @api {delete} /api/v1/users Delete user
   * @apiName DeleteUsers
   * @apiGroup Users
   *
   * @apiParam (Query string) {String} email User email
   * @apiHeader {String} token User token
   *
   * @apiExample {curl} Delete user:
   *     curl -v -X DELETE http://localhost:3000/api/v1/users?email=john.doe@example.com \
   *       -H "Token: abcdefjhij0123456789"
   *
   * @apiSuccessExample
   *     HTTP/1.1 204 No Content
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 400 Bad Request
   *     {
   *       "error": "Missing required field"
   *     }
   */
  delete(data, callback) {
    const email =
      typeof data.queryStringObject.email === 'string'
        ? data.queryStringObject.email.trim()
        : false;

    const tokenId =
      typeof data.headers.token === 'string' ? data.headers.token : false;

    if (email) {
      token.verify(tokenId, email, tokenIsValid => {
        if (tokenIsValid) {
          user.findByEmail(email, (err, userObject) => {
            if (!err && userObject) {
              user.delete(email, err => {
                if (!err) {
                  callback(204);
                } else {
                  callback(500, {error: 'Could not delete user'});
                }
              });
            } else {
              callback(400, {error: 'Specified user does not exist'});
            }
          });
        } else {
          callback(401);
        }
      });
    } else {
      callback(400, {error: 'Missing required field'});
    }
  },
});
