const controller = require('../../../../lib/controller');
const user = require('../../../models/user');
const menuItem = require('../../../models/menu-item');

module.exports = controller('/api/v1/cards', {
  /**
   * @api {put} /api/v1/cards Add or update a menu item in the card
   * @apiName PutCards
   * @apiGroup Cards
   *
   * @apiHeader {String} token User token
   *
   * @apiParam (Request body) {String} menuItemId Menu item ID
   * @apiParam (Request body) {Integer} quantity Quantity. Set to 0 to remove the item.
   *
   * @apiExample {curl} Create a new card:
   *     curl -v -X POST http://localhost:3000/api/v1/cards \
   *       -H "Token: abcdefjhij0123456789" \
   *       -d '{
   *         "menuItemId": "1",
   *         "quantity": 2
   *       }
   *     ]'
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     {
   *       "id": "1",
   *       "quantity": 2
   *       "name": 'Veggie',
   *       "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
   *       "amount": 9.99,
   *       "currency": "USD"
   *     }
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 400 Bad Request
   *     {
   *       "error": "Invalid or missing parameter"
   *     }
   */
  put(data, callback) {
    const tokenId =
      typeof data.headers.token === 'string' ? data.headers.token : false;

    const menuItemId =
      typeof data.payload.menuItemId === 'string'
        ? data.payload.menuItemId
        : false;

    const quantity =
      typeof data.payload.quantity === 'number' &&
      data.payload.quantity % 1 === 0 &&
      data.payload.quantity >= 0
        ? data.payload.quantity
        : false;

    if (menuItemId && quantity !== false) {
      user.findByTokenId(tokenId, (err, userObject) => {
        if (!err && userObject) {
          menuItem.findByID(menuItemId, (err, menuItemObject) => {
            if (!err && menuItemObject) {
              const card =
                userObject.card && typeof userObject.card === 'object'
                  ? userObject.card
                  : {};
              if (quantity === 0) {
                delete card[menuItemId];
              } else {
                card[menuItemId] = quantity;
              }
              userObject.card = card;
              user.update(userObject.email, userObject, err => {
                if (!err) {
                  if (quantity === 0) {
                    callback(201);
                  } else {
                    callback(200, {
                      ...menuItemObject,
                      quantity,
                    });
                  }
                } else {
                  callback(500, {error: 'Failed to add menu item in card'});
                }
              });
            } else {
              callback(404, {error: 'Menu item does not exist'});
            }
          });
        } else {
          callback(401);
        }
      });
    } else {
      callback(400, {error: 'Invalid or missing parameters'});
    }
  },

  /**
   * @api {get} /api/v1/cards Get all card items
   * @apiName GetCards
   * @apiGroup Cards
   *
   * @apiHeader {String} token User token
   *
   * @apiExample {curl} Get menu items:
   *     curl -v -X GET http://localhost:3000/api/v1/cards \
   *       -H "Token: abcdefjhij0123456789"
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     [
   *       {
   *         "id": "1",
   *         "quantity": 2
   *         "name": 'Veggie',
   *         "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
   *         "amount": 9.99,
   *         "currency": "USD"
   *       },
   *       {
   *         "id": "2",
   *         "quantity": 1,
   *         "name": "ExtravaganZZa",
   *         "description": "Loads of pepperoni, ham, savory Italian sausage, ground beef, fresh onions, fresh green peppers, fresh mushrooms and black olives with extra cheese.",
   *         "amount": 10.99,
   *         "currency": "USD"
   *       }
   *     ]
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 401 Unauthorized
   */
  get(data, callback) {
    const tokenId =
      typeof data.headers.token === 'string' ? data.headers.token : false;

    user.findByTokenId(tokenId, (err, userObject) => {
      if (!err && userObject) {
        const card =
          userObject.card && typeof userObject.card === 'object'
            ? userObject.card
            : {};
        menuItem.find((err, menuItemList) => {
          if (!err && menuItemList) {
            callback(
              200,
              menuItemList.reduce((acc, menuItemObject) => {
                const quantity = card[menuItemObject.id];
                if (quantity) {
                  acc.push({
                    ...menuItemObject,
                    quantity,
                  });
                }
                return acc;
              }, []),
            );
          } else {
            callback(500, {error: 'Failed to load menu'});
          }
        });
      } else {
        callback(401);
      }
    });
  },

  /**
   * @api {delete} /api/v1/cards Delete the content of the card
   * @apiName DeleteCards
   * @apiGroup Cards
   *
   * @apiHeader {String} token User token
   *
   * @apiExample {curl} Delete card item:
   *     curl -v -X DELETE http://localhost:3000/api/v1/cards \
   *       -H "Token: abcdefjhij0123456789"
   *
   * @apiSuccessExample
   *     HTTP/1.1 204 No Content
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 401 Unauthorized
   */
  delete(data, callback) {
    const tokenId =
      typeof data.headers.token === 'string' ? data.headers.token : false;

    user.findByTokenId(tokenId, (err, userObject) => {
      if (!err && userObject) {
        user.emptyCard(userObject, err => {
          if (!err) {
            callback(201);
          } else {
            callback(500, {error: 'Failed to delete card'});
          }
        });
      } else {
        callback(401);
      }
    });
  },
});
