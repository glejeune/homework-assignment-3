const controller = require('../../../../lib/controller');
const token = require('../../../models/token');
const menuItem = require('../../../models/menu-item');

module.exports = controller('/api/v1/menu-items', {
  /**
   * @api {get} /api/v1/menu-items Get menu items list
   * @apiName GetMenuItems
   * @apiGroup MenuItems
   *
   * @apiHeader {String} token User token
   *
   * @apiExample {curl} Get menu items list:
   *     curl -v -X GET http://localhost:3000/api/v1/menu-items \
   *       -H "Token: abcdefjhij0123456789"
   *
   * @apiSuccessExample
   *     HTTP/1.1 200 OK
   *     [
   *       {
   *         "id": '1',
   *         "name": 'Veggie',
   *         "description": "A medley of fresh green peppers, onion, tomatoes, mushrooms, and olives.",
   *         "amount": 9.99,
   *         "currency": "USD"
   *       },
   *       ...
   *     ]
   *
   * @apiErrorExample {json}
   *     HTTP/1.1 401 Unauthorized
   */
  get(data, callback) {
    const tokenId =
      typeof data.headers.token === 'string' ? data.headers.token : false;

    token.findByID(tokenId, (err, tokenObject) => {
      if (!err && tokenObject) {
        menuItem.find((err, menu) => {
          if (!err && menu && menu.length > 0) {
            callback(200, menu);
          } else {
            callback(500);
          }
        });
      } else {
        callback(401);
      }
    });
  },
});
