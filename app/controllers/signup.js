const controller = require('../../lib/controller');
const template = require('../../lib/template');

module.exports = controller('/signup', {
  get(data, callback) {
    template.render('signup.html', {head: {title: 'Sign up'}}, callback);
  },
});
